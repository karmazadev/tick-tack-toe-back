<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register'])
    ->name('register');
Route::post('login', [AuthController::class, 'login'])
    ->name('login');

Route::middleware('auth:api')
    ->group(function() {
        //@TODO: remove test data
        Route::get('/', function () {
            return json_encode(['a' => 'a', 'b']);
        });

        Route::get('user', [UserController::class, 'show']);

        Route::get('game', [GameController::class, 'show']);
    });

