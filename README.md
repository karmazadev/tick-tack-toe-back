## Requirements
- PHP > 7.3.0
- Node.js > 14.15

## Installing
- composer install
- npm install

## Running
- php artisan serve