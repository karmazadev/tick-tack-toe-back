<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\GameService;

class GameController extends Controller
{
    private $gameService;

    public function __construct(GameService $gameService)
    {
       $this->gameService = $gameService;
    }

    public function show()
    {
        return $this->api_response($this->gameService
            ->getGame());
    }
}
