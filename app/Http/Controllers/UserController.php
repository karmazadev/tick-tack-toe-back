<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function show() {
        $user = Auth::user();

        if (!$user) {
            return $this->api_response(null, 'User not found', 0, 404);
        }

        return $this->api_response($user);
    }
}
