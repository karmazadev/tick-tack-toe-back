<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'string|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed'
        ]);

        $user = User::create([
            'name' => $data['name'],
            'password' => bcrypt($data['password']),
            'email' => $data['email']
        ]);

        return $this->api_response([
            'token' => $user->createToken('API Token')
                ->plainTextToken,
        ]);
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!Auth::attempt($data)) {
            return $this->api_response(null, 'User not found', 0, 404);
        }

        return $this->api_response([
            'token' => auth()->user()
                ->createToken('API Token')
                ->plainTextToken
        ]);
    }

    public function logout()
    {
        auth()->user()
            ->tokens()
            ->delete();

        return $this->api_response(null);
    }
}
