<?php

namespace App\Services;

use App\Models\Game;

class GameService {
    public function getGame()
    {
        $gameId = request()->session()
            ->get('gameId', null);

        if ($gameId) {
            return $this->getGameById($gameId);
        }

        return $this->createNewGame();
    }

    private function getGameById($id)
    {
        $game = Game::find($id);

        if (!$game) {
            return $this->createNewGame();
        }

        return $game;
    }

    private function createNewGame()
    {
        $game = Game::create([
            'user_id' => auth()->user()->id,
            'board' => $this->createNewBoard(),
        ]);

        session(['gameId' => $game->id]);

        return $game;
    }

    private function createNewBoard()
    {
        return [
            [null, null, null],
            [null, null, null],
            [null, null, null],
        ];
    }
}
