<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameLog extends Model
{
    public $timestamps = false;


    public function game() {
        return $this->belongsTo('App\Models\Game', 'game_id');
    }
}
