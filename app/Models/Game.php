<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'user_id',
        'board',
    ];

    protected $casts = [
        'board' => 'array',
    ];

    public function logs()
    {
        return $this->hasMany('App\Models\GameLog', 'game_id');
    }

    public function cells()
    {
        return $this->hasMany('App\Models\GameCell', 'game_id');
    }
}
