<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameCell extends Model
{
    protected $fillable = [
        'game_id',
        'cell',
        'player',
    ];

    public function game() {
        return $this->belongsTo('App\Models\Game', 'game_id');
    }
}
