<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->json('board');
            $table->timestamps();

            $table->index(['user_id']);
        });

        Schema::create('game_cells', function (Blueprint $table) {
            $table->id();
            $table->foreignId('game_id');
            $table->string('cell');
            $table->char('player');
            $table->timestamp('created_at')
                ->useCurrent();

            $table->index(['game_id']);
        });

        Schema::create('game_logs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('game_id');
            $table->char('player');
            $table->string('cell');
            $table->text('text');
            $table->timestamp('created_at')
                ->useCurrent();

            $table->index(['game_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
        Schema::dropIfExists('game_logs');
    }
}
